##############
# IF YOU WANNA TEST THAT SCRIPT I RECOMMENDED UNCOMMENT LINES 49 and 57
##############
from argparse import ArgumentParser
from sqlite3 import connect
import time

create_tracks_table = """
CREATE TABLE IF NOT EXISTS tracks(
id_track STRING PRIMARY KEY,
id_performer STRING,
author VARCHAR(50),
name VARCHAR(50)
);
"""
create_index_tracks = """CREATE INDEX IF NOT EXISTS tracks_id_index ON tracks("id_track")"""

create_useCases_table = """
CREATE TABLE IF NOT EXISTS useCases(
id_user STRING,
id_track STRING,
time TIMESTAMP
)
"""

create_index_useCases = """CREATE INDEX IF NOT EXISTS useCases_track_id_index ON useCases("track_id")"""

def main():
    setTime = time.time()
    parser = ArgumentParser(description='desc')
    parser.add_argument('--path', dest='path', required=True)
    parser.add_argument('--tracks', dest='tracks', required=False)
    parser.add_argument('--useCases', dest='useCases', required=False)

    args = parser.parse_args()

    conn = connect(args.path)
    curs = conn.cursor()

    curs.execute(create_tracks_table)
    curs.execute(create_index_tracks)
    curs.execute(create_useCases_table)
    curs.execute(create_index_useCases)

    with open(args.tracks, 'r', encoding='ISO-8859-1') as item:
        for index, single_line in enumerate(item):
            id_performance, id_track, author, name = single_line.split('<SEP>')
            curs.execute('INSERT INTO tracks VALUES (?,?,?,?)', (id_performance, id_track, author, name))
            # if index > 50000:
            #     break
        conn.commit()

    with open(args.useCases, 'r', encoding='ISO-8859-1') as item:
        for index, single_line in enumerate(item):
            id_user, id_track, date = single_line.split('<SEP>')
            curs.execute('INSERT INTO useCases VALUES (?,?,?)', (id_user, id_track, date))
            # if index > 50000:
            #     break
        conn.commit()

    best_musics = conn.execute("""
    SELECT author, count(*) as song_plays 
    FROM tracks
    JOIN useCases ON tracks.id_performer = useCases.id_track
    GROUP BY tracks.id_performer
    ORDER BY song_plays DESC
    LIMIT 5""")

    print('Best musics:')
    for row in best_musics:
        print(row[0])

    best_artist = conn.execute("""
    SELECT p.author, p.useCases FROM (
    SELECT t.author as author, count(*) as useCases FROM tracks t
    JOIN useCases ON useCases.id_track = t.id_performer
    GROUP BY author
    ORDER BY useCases DESC
    LIMIT 1
    ) p
    """).fetchone()

    print('Best artist:')
    print(best_artist[0])
    print('Operation time:')
    print(time.time() - setTime)

if __name__ == '__main__':
    main()
